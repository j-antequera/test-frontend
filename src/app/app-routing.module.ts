import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'money',
    pathMatch: 'full'
  },
  {
    path: 'money',
    loadChildren: () => import('./pages/money/money.module').then( m => m.MoneyPageModule)
  },
  {
    path: 'deposits',
    loadChildren: () => import('./pages/deposits/deposits.module').then( m => m.DepositsPageModule)
  },
  {
    path: 'details',
    loadChildren: () => import('./pages/details/details.module').then( m => m.DetailsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
