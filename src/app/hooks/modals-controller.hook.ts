import { Injectable } from '@angular/core';
import { ModalController, PopoverController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ModalsControllerHook {

  modalSelect;
  popoverSelect;

  constructor(
    private modalController: ModalController,
    private popoverController: PopoverController
  ) { }

  async openModal(component, style, modalParams) {
    this.modalSelect = await this.modalController.create({
      component,
      cssClass: style,
      showBackdrop: false,
      componentProps: { modalParams }
    });
    await this.modalSelect.present();
  }

  closeModal() {
    this.modalSelect.dismiss({
      dismissed: true
    });
  }

  async openPopover(component, event, style, modalParams) {
    this.popoverSelect = await this.popoverController.create({
      component,
      cssClass: style,
      showBackdrop: false,
      mode: 'md',
      event,
      componentProps: { modalParams }
    });
    await this.popoverSelect.present();
  }

  async openPopoverToModal(component, style, modalParams) {
    this.popoverSelect = await this.popoverController.create({
      component,
      cssClass: style,
      showBackdrop: true,
      componentProps: { modalParams },
    });
    await this.popoverSelect.present();
  }

  async closePopover() {
    await this.popoverController.dismiss();
  }

}
