import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastAlert {

  async toastSuccess(message: string) {
    const toast = await this.toastController.create({
      message,
      cssClass: 'toast-class',
      duration: 3000,
      color: 'success',
      mode: 'ios',
      buttons: [
        {
          side: 'start',
          icon: 'checkmark-circle',
        }
      ]
    });
    toast.present();
  }

  async toastFailure(message: string) {
    const toast = await this.toastController.create({
      message,
      cssClass: 'toast-class',
      duration: 3000,
      color: 'danger',
      mode: 'ios',
      buttons: [
        {
          side: 'start',
          icon: 'close-circle',
        }
      ]
    });
    toast.present();
  }

  async toastWarning(message: string) {
    const toast = await this.toastController.create({
      message,
      cssClass: 'toast-class',
      duration: 3000,
      color: 'warning',
      mode: 'ios',
      buttons: [
        {
          side: 'start',
          icon: 'close-circle',
        }
      ]
    });
    toast.present();
  }

  constructor(
    private toastController: ToastController,
  ) { }

}
