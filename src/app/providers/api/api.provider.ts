import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiModule } from '../../modules/api/api.module';

@Injectable({
  providedIn: 'root'
})

export class ApiProvider {

  constructor(private apiModule: ApiModule, private http: HttpClient) { }

  read(id: number) {
    const path = 'api/read';
    const method = 'get';
    const body = { extensionId: id };

    const options = this.apiModule.run({ path, method, body });
    return this.http[options.method]<any>(options.url, options.data, {headers: options.headers});
  }

  reads() {
    const path   = 'api/read';
    const method = 'get';
    const body   = {};

    const options = this.apiModule.run({ path, method, body });
    return this.http[options.method]<any>(options.url, options.data, {headers: options.headers});
  }

  create(requestOptions) {
    const path   = 'api/insert';
    const method = 'post';
    const body: {
      denomination: string
    } = {
      denomination: requestOptions.denomination
    };

    const options = this.apiModule.run({ path, method, body });
    return this.http[options.method]<any>(options.url, options.data, {headers: options.headers});
  }

  update(requestOptions) {
    const path = 'api/update';
    const method = 'put';
    const params = { extensionId: requestOptions.extensionId };
    const body: {
      denomination: string
    } = {
      denomination: requestOptions
    };

    const options = this.apiModule.run({ path, method, body, params });
    return this.http[options.method]<any>(options.url, options.data, {headers: options.headers});
  }

  remove(requestOptions) {
    const path = 'api/delete';
    const method = 'delete';
    const params = requestOptions;

    const options = this.apiModule.run({ path, method, params });
    return this.http[options.method]<any>(options.url, {headers: options.headers, params: options.query});
  }

}
