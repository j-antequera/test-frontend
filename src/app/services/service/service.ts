import { Injectable } from '@angular/core';
import { ApiProvider } from '../../providers/api/api.provider';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(
    private apiProvider: ApiProvider,
  ) { }

  public findById(id: any): Observable<any> {

    return this.apiProvider.read(id);
  }

  public findAll(): Observable<any> {

    return this.apiProvider.reads();
  }
 
  public insert(data: any): Observable<any> {

    return this.apiProvider.create(data)
  }

  public update(Id: number, Update: any): Observable<any> {

    return this.apiProvider.update({Id, Update});
  }
  
  public delete(Id): Observable<any> {

    return this.apiProvider.remove(Id);
  }

}
