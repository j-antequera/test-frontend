import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  appPages = [
    { title: 'Monedas', url: '/money', icon: 'cash' },
    { title: 'Depositos', url: '/deposits', icon: 'paper-plane' },
    { title: 'Detalles', url: '/details', icon: 'podium' },
  ];

  labels = ['Notes', 'Work'];

  constructor() {}
}
