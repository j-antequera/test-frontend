import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastAlert } from 'src/app/hooks/toast-alert.hook';
import { ApiService } from 'src/app/services/service/service';

@Component({
  selector: 'app-fv-add',
  templateUrl: './fv-add.component.html',
  styleUrls: ['./fv-add.component.scss'],
})
export class FvAddComponent implements OnInit {

  label = 'Denominación';
  denominations = [
    { text: 'Seleccionar', value: 0 }, 
    { text: 'Cincuenta', value: 50 }, 
    { text: 'Cien', value: 100 }, 
    { text: 'Doscientos', value: 200 }, 
    { text: 'Quinientos', value: 500 }, 
    { text: 'Mil', value: 1000 }, 
  ];
  button = 'Aceptar';
  error = "¡Seleccione una denominacion!"

  submit: boolean;

  money: FormGroup;

  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder,
    private toastAlert: ToastAlert,
    ) { }

  ngOnInit() {
    this.money = this.formBuilder.group({
      denomination: new FormControl(0, Validators.min(50))
    })
  }

  submitMoney() {
    this.submit = true;

    if (this.money.invalid) { return; }
    
    this.apiService.insert(this.money.value).subscribe(moneys => {
      if (moneys) {
        this.toastAlert.toastSuccess('Se guardaron corectamete');
      }
    });
  }

}
