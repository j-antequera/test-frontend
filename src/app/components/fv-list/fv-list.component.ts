import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/service/service';


@Component({
  selector: 'app-fv-money',
  templateUrl: './fv-list.component.html',
  styleUrls: ['./fv-list.component.scss'],
})
export class FvListComponent implements OnInit {

  moneyInformations = [];
  typeMoney = 'peos';


  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.findAll().subscribe(moneys => {
      this.moneyInformations = moneys;
    });
  }

  parserDenomination(value) {
    if (value == 50) {
      return 'fifty';
    }
    if (value == 100) {
      return 'one-hundred';
    }
    if (value == 200) {
      return 'two-hundred';
    }
    if (value == 500) {
      return 'five-hundred';
    }
    if (value == 1000) {
      return 'one-thousand';
    }
  }

}
