import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/service/service';

@Component({
  selector: 'app-fv-generals',
  templateUrl: './fv-generals.component.html',
  styleUrls: ['./fv-generals.component.scss'],
})
export class FvGeneralsComponent implements OnInit {

  qualityOfCoins = 'Cantidad de monedas';
  qualityOfMoney = 'Cantidad de dinero';
  coinsPerDenomination = 'Cantidad de monedas por denominación';
  moneyPerDenomination = 'Cantidad de dinero por denominación';

  denominations = [
    { text: 'Cincuenta', value: 50 }, 
    { text: 'Cien', value: 100 }, 
    { text: 'Doscientos', value: 200 }, 
    { text: 'Quinientos', value: 500 }, 
    { text: 'Mil', value: 1000 }, 
  ];

  moneyInformations = [];

  totalCoinsPerDenomination: number;
  totalMoneyPerDenomination: number;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.findAll().subscribe(moneys => {
      this.moneyInformations = moneys;
    });

  }

  cashCounter() {
    return this.moneyInformations.reduce((count, money) => count += +money.denomination, 0);
  }

  coinsCounterPerDenomination(money) {
    const denomination = money.detail.value;
    this.totalCoinsPerDenomination = this.moneyInformations.filter(money => money.denomination == denomination).length;
  }

  moneyCounterPerDenomination(money) {
    const denomination = money.detail.value;
    this.totalMoneyPerDenomination = this.moneyInformations.reduce((count, money) => {
      return money.denomination == denomination ? count += +money.denomination : count;
    }, 0);
  }

}
