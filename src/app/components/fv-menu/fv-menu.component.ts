import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fv-menu',
  templateUrl: './fv-menu.component.html',
  styleUrls: ['./fv-menu.component.scss'],
})
export class FvMenuComponent implements OnInit {

  appPages = [
    { title: 'Inicio', url: '/home', icon: 'paper-plane' },
  ];
  labels = ['Notes', 'Work'];

  constructor() { }

  ngOnInit() {}

}
